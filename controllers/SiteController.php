<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    //
    //
    //CONSULTAS 
    //
    //Consulta 1a
    public function actionConsulta1a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("edad")->distinct(),
            'pagination'=>[
                    'pageSize'=>10,
            ]
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
           ]);
    }
    //Consulta 1
    public function actionConsulta1(){
        $numero = Yii::$app->db
            ->createCommand('SELECT count(DISTINCT edad) FROM ciclista')
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>'SELECT DISTINCT edad FROM ciclista',

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetir)",
            "sql"=>"SELECT DISTINCT edad FROM ciclistas",
        ]);
    }
    
    //Consulta 2a
    public function actionConsulta2a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("edad")->distinct()->where("nomequipo='Artiach'"),
            'pagination'=>[
                    'pageSize'=>10,
            ]
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach';",
        ]);
    }
    //Consulta 2
    public function actionConsulta2(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo = 'Artiach'")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach';",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach';",
        ]);
    }
    
    //Consulta 3a
    public function actionConsulta3a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select ("edad")->distinct()->where("nomequipo='Artiach' OR nomequipo='Amore vita'"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR 'Amore Vita';",
        ]);
    }
    //Consulta 3
    public function actionConsulta3(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo = 'Artiach' OR 'Amore Vita'")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR 'Amore Vita';",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR 'Amore Vita';",
        ]);
    }
    
    //Consulta 4a
    public function actionConsulta4a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select ("dorsal")->distinct()->where("edad>30 OR edad<25"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad>30 OR edad<25;",
        ]);
    }
    //Consulta 4
    public function actionConsulta4(){
        $numero = Yii::$app->db
            ->createCommand('SELECT count(DISTINCT dorsal) FROM ciclista WHERE edad>30 OR edad<25')
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad>30 OR edad<25;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad>30 OR edad<25;",
        ]);
    }
    
    //Consulta 5a
    public function actionConsulta5a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select ("dorsal")->distinct()->where("(edad BETWEEN 28 AND 32) AND nomequipo='Banesto'"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND nomequipo='Banesto';",
        ]);
    }
    //Consulta 5
    public function actionConsulta5(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT dorsal) FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND nomequipo='Banesto'")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT dorsal FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND nomequipo='Banesto';",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND nomequipo='Banesto';",
        ]);
    }
    
    //Consulta 6a
    public function actionConsulta6a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select ("nombre")->distinct()->where("CHAR_LENGTH(nombre)>8"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indicar el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8;",
        ]);
    }
    //Consulta 6
    public function actionConsulta6(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT nombre) FROM ciclista WHERE CHAR_LENGTH(nombre)>8")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Indicar el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8;",
        ]);
    }
    
    //Consulta 7a
    public function actionConsulta7a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select ("UPPER(nombre) nombre, dorsal") ->distinct(),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>[ "nombre","dorsal"],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo "
            . "denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT UPPER(nombre) AS NombreMayus,dorsal FROM ciclista;",
        ]);
    }
    //Consulta 7
    public function actionConsulta7(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT UPPER(nombre) , dorsal) FROM ciclista")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT UPPER(nombre) AS NombreMayus,dorsal FROM ciclista;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['NombreMayus' ,
                'dorsal'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo "
            . "denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT UPPER(nombre) AS NombreMayus,dorsal FROM ciclista;",
        ]);
    }
    
    //Consulta 8a
    public function actionConsulta8a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Lleva::find() ->select ("dorsal")->distinct()->where("código='MGE'"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE';",
        ]);
    }
    //Consulta 8
    public function actionConsulta8(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT dorsal) FROM lleva WHERE código='MGE'")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE';",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE';",
        ]);
    }
    
    //Consulta 9a
    public function actionConsulta9a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Puerto::find() ->select ("nompuerto")->distinct()->where("altura>1500"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500;",
        ]);
    }
    //Consulta 9
    public function actionConsulta9(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT nompuerto) FROM puerto WHERE altura>1500")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500;",
        ]);
    }
    
    //Consulta 10a
    public function actionConsulta10a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Puerto::find() ->select ("dorsal")->distinct()->where("(altura BETWEEN 1800 AND 3000) OR pendiente>8"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente "
            . "sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE (altura BETWEEN 1800 AND 3000) OR pendiente>8;",
        ]);
    }
    //Consulta 10
    public function actionConsulta10(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT dorsal) FROM puerto WHERE (altura BETWEEN 1800 AND 3000) OR pendiente>8")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT dorsal FROM puerto WHERE (altura BETWEEN 1800 AND 3000) OR pendiente>8;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente "
            . "sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE (altura BETWEEN 1800 AND 3000) OR pendiente>8;",
        ]);
    }
    
    //Consulta 11a
    public function actionConsulta11a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Puerto::find() ->select ("dorsal")->distinct()->where("(altura BETWEEN 1800 AND 3000) AND pendiente>8"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente "
            . "sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE (altura BETWEEN 1800 AND 3000) AND pendiente>8;",
        ]);
    }
    //Consulta 11
    public function actionConsulta11(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT dorsal) FROM puerto WHERE (altura BETWEEN 1800 AND 3000) AND pendiente>8")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT dorsal FROM puerto WHERE (altura BETWEEN 1800 AND 3000) AND pendiente>8;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente "
            . "sea mayor que 8 Y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE (altura BETWEEN 1800 AND 3000) AND pendiente>8;",
        ]);
    }
}
